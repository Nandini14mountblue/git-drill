Create a directory called git_sample_project and cd to git_sample_project
mkdir git_sample_project
cd git_sample_project

Initialize a git repo.
git init

Create the following files a.txt, b.txt, c.txt
touch a.txt b.txt c.txt

Add a.txt and b.txt to the staging area
git add a.txt b.txt

Run git status. Understand what it says.
git status
status of repository- It is commit or not.

Commit a.txt and b.txt with the message "Add a.txt and b.txt"
git commit -m "Add a.txt and b.txt"

Run git status.
git status

Run git log
git log

Add and Commit c.txt
git add c.txt
git commit

 
